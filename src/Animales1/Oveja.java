/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animales1;

/**
 *
 * @author nacho
 */
public class Oveja extends Animal {

    static int numOvejas;
    
    private int idOveja;
    
    public Oveja(String nombre) {
        super(nombre);
        sexo="hembra";
        alimentacion="hierbas";
        nombreSonido="balido";
        color="blanco";
        idOveja=numOvejas;
        numOvejas++;
    }
    
    public void hablar() {
        System.out.println("Beeeeeee, beeeeeeee, soy una oveeeeeeeeeeejaaaaa");
        System.out.println("El nombre del sonido que hago es " + nombreSonido);
    }
    
    public void getId() {
        System.out.println("Mi identificador es " + idOveja);
    }
    
    public static void getNumOvejas() {
        System.out.println("El número de ovejas es " + numOvejas);
    }
            
   
    
}
