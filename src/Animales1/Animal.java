/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animales1;

/**
 *
 * @author nacho
 */
public class Animal {

    static int numero;
    
    private String nombre;
    private int numPatas;
    private int numOjos;
    protected String sexo;
    protected String alimentacion;
    protected String nombreSonido;
    protected String color;
    
    public Animal(String nombre) {
        this.nombre=nombre;
        numPatas=4;
        numOjos=2;
        numero++;
    }
    
    public void mostrar() {
        System.out.println("Tengo " + numPatas + " patas");
        System.out.println("Tengo " + numOjos + " ojos");
        System.out.println("Me alimento de " + alimentacion + " principalmente");
        System.out.println("Mi color es " + color);
    }
    
    public void setColor(String color) {
        this.color=color;
    }
    
    public String getColor() {
        return color;
    }
    
    public static void getNumero() {
        System.out.println("El número total de animales es " + numero);
    }
            
   
    
}
