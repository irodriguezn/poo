/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animales1;

/**
 *
 * @author nacho
 */
public class AppAnimales {
    public static void main(String[] args) {
        // TODO code application logic here
        Oveja ov1=new Oveja("Lucera");
        ov1.setColor("negro");
        ov1.mostrar();
        ov1.getId();
        Oveja.getNumOvejas();
        Oveja ov2=new Oveja("Escarlata");
        ov2.setColor("rojo");
        ov2.mostrar();
        ov2.getId();
        Oveja.getNumOvejas();
        ov2.hablar();
        Lobo l1=new Lobo("Caperucito");
        l1.mostrar();
        l1.getId();
        Lobo.getNumLobos();
        Animal.getNumero();
    }
}
