/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animales1;

/**
 *
 * @author nacho
 */
public class Lobo extends Animal {
    private int idLobo;
    private static int numLobos;
    
    public Lobo(String nombre) {
        super(nombre);
        idLobo=numLobos;
        numLobos++;
        color="marron";
        sexo="macho";
        alimentacion="ovejas";
        nombreSonido="aullido";
    }
    
    public void hablar() {
        System.out.println("Aaaaauuuuuuuuuuuuuuuu");
    }
    
    public void getId() {
        System.out.println("Mi identificador es " + idLobo);
    }
    
    public static void getNumLobos() {
        System.out.println("El número de lobos es " + numLobos);
    }
}
